<?php


namespace App\Transformers;


use Illuminate\Notifications\DatabaseNotification;
use League\Fractal\TransformerAbstract;

class NotificationTransformer extends  TransformerAbstract
{

    /**注意这里我们需要格式化的模型是Illuminate\Notifications\DatabaseNotification;
     * @param DatabaseNotification $notification
     */
    public function transform(DatabaseNotification $notification){
        return [
            'id'=>$notification->id,
            'type'=>$notification->type,
            'data'=>$notification->data,
            'read_at'=>$notification->read_at?$notification->read_at->toDataTimeString():null,
            'created_at'=>(string)$notification->created_at,
            'updated_at'=>(string)$notification->updated_at,
        ];
    }
}
