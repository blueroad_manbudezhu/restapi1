<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable=['type','path'];

    //定义关联模型
    public function user()
    {
        $this->belongsTo(User::class);
    }
}
