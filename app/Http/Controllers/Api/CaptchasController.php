<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CaptchaRequest;
use Gregwar\Captcha\CaptchaBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CaptchasController extends Controller
{
    //
    public function store(CaptchaRequest $request)
    {
        $key = 'captchas_' . str_random(15);
        $builder = new CaptchaBuilder();
        $captcha = $builder->build();
        $expire = now()->addMinutes(5);
        $status = [
            'captch_key' => $key,
            'expire_at' => $expire,
            'captcha_image'=>$captcha->inline(),
        ];
        //存入缓存
        Cache::put($key,['phone'=>$request->phone,'code'=>$captcha->getPhrase()],$expire);
        //返回状态值
        return $this->response->array($status)->setStatusCode(201);
    }
}
