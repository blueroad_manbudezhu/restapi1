<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\UserRequest;
use App\Models\Image;
use App\Models\User;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class UsersController extends Controller
{
    /**接口文档
     * @param 传入参数 name password verify_key verify_code
     * @return 返回参数
     */
    public function store(UserRequest $request)
    {
        //先验证缓存的phone code是否一致,如一致存入数据库,返回结果
        $cachekey=$request->verify_key;
        $inputCode=$request->verify_code;
        $data=Cache::get($cachekey);
        //需要判断两种异常:一种缓存失效,另一种缓存验证码无效
        if(!$data){
           return  $this->response->error('缓存已经失效',422);
        }
        //比较两个字符串，无论它们是否相等，本函数的时间消耗是恒定的。防止时序攻击
        if(!hash_equals($data['code'],$request->verify_code)){
            return $this->response->errorUnauthorized('验证码无效');
        }
        /*入库*/
        $user =new User();
        $user->name=$request->name;
        $user->password=bcrypt($request->password);
        $user->phone=$data['phone'];
        $user->save();
        // 这里需要清除缓存
        Cache::forget($cachekey);
        return $this->response->item($user,new UserTransformer())
        ->setMeta([
            'access_token'=>\Auth::guard('api')->fromUser($user),
            'token_type'=>'Bearder',
            'expires_in'=>\Auth::guard('api')->factory()->getTTL()*60
        ])->setStatusCode(201);
    }

    /**
     * 返回当前登录用户
     * @return string
     */
    public function me()
    {
        //Dingo\Api\Routing\Helpers 他提供了user方法.
        // return  auth('api')->user();
        return $this->response->item(auth('api')->user(),new UserTransformer());
    }


    /**
     * 用户资料更新接口
     * 传入参数:
     * avatar_image_id 图片在image表中的id
     * 其他参数可选
     */
    public function update(UserRequest $request)
    {
        //获取当前登录的用户
        $user=$this->user();
        $attributes=$request->only(['name','email','introduction']);
        //判断是否修改头像
        if($request->avatar_image_id){
            $image=Image::find($request->avatar_image_id);
            $attributes['avatar']=$image->path;
        }
        //更新数据
        $user->update($attributes);
        return $this->response->item($user,new UserTransformer());
    }

    /**活跃用户接口
     * @param User $user
     * @return \Dingo\Api\Http\Response
     */
    public function activedIndex(User $user)
    {
        return  $this->response->collection($user->getActiveUsers());
    }
}
