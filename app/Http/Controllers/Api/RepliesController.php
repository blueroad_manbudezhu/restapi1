<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ReplyRequest;
use App\Models\Reply;
use App\Models\Topic;
use App\Models\User;
use App\Transformers\ReplyTransformer;
use Illuminate\Http\Request;

class RepliesController extends Controller
{
    //
    public function store(ReplyRequest $request, Topic $topic, Reply $reply)
    {
        $reply->content = $request->input('content');
        $reply->topic_id = $topic->id;
        $reply->user_id = $this->user()->id;
        $reply->save();
        return $this->response->item($reply, new ReplyTransformer())->setStatusCode(201);
    }

    /**删除回复
     * @param Topic $topic
     * @param Reply $reply
     */
    public function destroy(Topic $topic, Reply $reply)
    {
        //传入参数topic_id 文章id, reply_id 回复id
        if ($reply->topic_id != $topic->id) {
            return $this->response->errorBadRequest();
        }
        $this->authorize('destroy', $reply);
        $reply->delete();
        return $this->response->noContent();
    }

    //某个文章的回复列表
    public function index(Topic $topic)
    {
        //这里需要加个参数判断,如果不传入参数或参数不正确返回错误信息
        $replies = $topic->replies()->paginate(20);
        return $this->response->paginator($replies, new ReplyTransformer());
    }

    public function userIndex(User $user)
    {
        $replies=$user->replies()->paginate(20);
        return $this->response->paginator($replies,new ReplyTransformer());
    }
}
