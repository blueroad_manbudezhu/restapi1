<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\VerifyCodesRequest;
use Illuminate\Support\Facades\Cache;
use Overtrue\EasySms\EasySms;
use Overtrue\EasySms\Exceptions\NoGatewayAvailableException;

class VerifyCodesController extends Controller
{

    /**api 短信验证码
     * @param phone[电话]
     * @return mixed
     */
    public function store(VerifyCodesRequest $request,EasySms $easySms)
    {
        //验证图形验证码
        $data=\Cache::get($request->captcha_key);
        if(!$data){
            return $this->response->error('缓存失效',422);
        }
        if(!hash_equals($data['code'],$request->captcha_code)){
            Cache::forget($request->captcha_key);
            return $this->response->errorUnauthorized('验证码不正确');
        }
        //发送短信
        $phone=$data['phone'];
        $code=str_pad(random_int(1,9999),4,0,STR_PAD_LEFT);
        try{
            $result=$easySms->send($phone,[
                'template'=>'SMS_109340195',
                'data'=>[
                    'code'=>$code
                ],
            ]);
        }catch (NoGatewayAvailableException $exception){
            $message=$exception->getException('aliyun')->getMessage();
        }

        //缓存短信验证码内容
            $cacheKey='verifyCode_'.str_random(15);
            $cacheExpire=now()->addMinute(5);
            Cache::put($cacheKey,['phone'=>$phone,'code'=>$code],$cacheExpire);
        //返回响应
        return $this->response->array([
            'key'=>$cacheKey,
            'expired_at'=>$cacheExpire->toDateTimeLocalString(),
        ])->setStatusCode(201);

    }
}
