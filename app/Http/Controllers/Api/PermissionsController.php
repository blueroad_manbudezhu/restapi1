<?php

namespace App\Http\Controllers\Api;

use App\Transformers\PermisssionTransformer;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    //
    public function index()
    {
        $pemissions=$this->user->getAllPermissions();
        return $this->response->collection($pemissions,new PermisssionTransformer());
    }
}
