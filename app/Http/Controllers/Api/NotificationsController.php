<?php

namespace App\Http\Controllers\Api;

use App\Transformers\NotificationTransformer;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    //
    public function index()
    {
        $notificaions = $this->user->notifications()->paginate(10);
        return $this->response->paginator($notificaions, new NotificationTransformer());
    }

    /**未读消息数量
     * @return mixed
     */
    public function stat()
    {
        return $this->response->array(
            ['unread_count' => $this->user()->notification_count]
        );
    }


    public function read()
    {
        $this->user()->markAsRead();
        return $this->response->noContent();
    }
}
