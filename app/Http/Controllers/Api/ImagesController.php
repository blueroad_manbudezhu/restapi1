<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ImageRequest;
use App\Models\Image;
use App\Transformers\ImageTransformer;
use Blues\basics\image\ImageUpload;

class ImagesController extends Controller
{
    //
    public function store(ImageRequest $request,ImageUpload $imageUpload,Image $image){
        $user=$this->user();
        $size=$request->type=='avatar'?362:1024;
        $result=$imageUpload->upload($request->image,str_plural($request->type),$user->id,$size);

        $image->path=$result;
        $image->type=$request->type;
        $image->user_id=$user->id;
        $image->save();

        return $this->response->item($image,new ImageTransformer())->setStatusCode(201);
    }
}
