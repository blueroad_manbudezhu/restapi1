<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\Api\AuthorizationRequest;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\SocialiteServiceProvider;
use App\Http\Requests\Api\AuthRequest;
use Tymon\JWTAuth\JWTAuth;

class AuthorizationController extends Controller
{
    //普通用户登录
    public function store(AuthRequest $request)
    {
        //传入参数有两种可能username可以为邮箱或电话
        $username=$request->username;
        filter_var($username,FILTER_VALIDATE_EMAIL)?$credentials['email']=$username:$credentials['phone']=$username;
        $credentials['password']=$request->password;
        if(!$token=\Auth::guard('api')->attempt($credentials)){
            return $this->response->errorUnauthorized('用户名密码不正确');
        }
        return $this->response->array([
            'access_token'=>$token,
            'token_type'=>'Bearer',
            'expires_in'=>\Auth::guard('api')->factory()->getTTL()*60,
        ])->setStatusCode(201);

    }
    /**
     * 该接口可以输入code或access_token
     */
    public function weixin($type,AuthorizationRequest $request)
    {
        if(!in_array($type,['weixin'])){
            return $this->response->errorBadRequest();
        }
        $driver = \Socialite::driver($type);
        try{
            //如果传递是code
            if($code=$request->code){
                /*获取的response数据
                {
                    "access_token": "22_UjakJ1Jl1DjrwzDTH9yCV3L5mQQS7reEuJBN0g5UWFs45tbUWlFi_6zUjJL3phpduAG2fzIbiOrbD5D1MFQ6Pg",
                    "expires_in": 7200,
                    "refresh_token": "22_C80ki-pGUeya-7ZknCvx2w0G_mlUa4UPR8ZYipSyOl887256Xwg9GXzgIa-A4sBd3_jYx2PuT5FDqYO1E4AVww",
                    "openid": "oelpW51cqC2jXnniZeR2GlHge9lQ",
                    "scope": "snsapi_userinfo"
                    }   */
                $response = $driver->getAccessTokenResponse($code);
                //array_get方法使用 .号从嵌套数组中获取值
                $token=array_get($response,'access_token');
            }else{
                $token=$request->access_token;
                if($type=='weixin'){
                    $driver->setOpenId($request->openid);
                }
            }
            $oauthUser=$driver->userFromToken($token);

        }catch(\Exception $e){
            return $this->response->errorUnauthorized('参数错误');
        }
        //根据获取的用户信息,去数据库中查找,如果存在返回查询结果,否则新建数据并返回
        switch($type){
            case 'weixin':
                //判断是用unionid还是openid查询了,优先用unionid查询
                $unionid=$oauthUser->offsetExists('unionid')?$oauthUser->offsetGet('unionid'):null;
                if($unionid){
                    $user=User::where('weixin_unionid',$unionid)->first();
                }else{
                    $user=User::where('weixin_openid',$oauthUser->getId())->first();
                }
                //如果没有$user数据
                if(!$user){
                    $user=User::create([
                        'name'=>$oauthUser->getNickname(),
                        'avatar'=>$oauthUser->getAvatar(),
                        'weixin_openid'=>$oauthUser->getId(),
                        'weixin_unionid'=>$unionid,
                    ]);
                }
                break;
        }
        return $this->respondWithToken($token);
    }

    /**
     * token 刷新
     */
    public function update(){
        $token=\Auth::guard('api')->refresh();
        return $this->respondWithToken($token);
    }
    /**
     * token删除
     */
    public function destory(){
        \Auth::guard('api')->logout();
        return $this->response->noContent();
    }

    /**
     * token 返回通用代码
     */
    public function respondWithToken($token){
        return $this->response->array([
            'access_token'=>$token,
            'token_type'=>'Bearer',
            'expires_in'=>\Auth::guard('api')->factory()->getTTL()*60
        ]
        );
    }
}
