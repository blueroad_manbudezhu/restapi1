<?php
/**
 * 基础控制器,引入dingo/api的相关方法
 */
namespace App\Http\Controllers\Api;

use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    //引入dingo的部分方法
    use Helpers;
}
