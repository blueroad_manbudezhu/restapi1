<?php

namespace App\Http\Controllers\Demo;

use App\Http\Controllers\Api\Controller;
use Blues\Demo;

class TestController extends Controller
{
    //
    public function index()
    {
        $obj=new Demo();
        echo $obj->test();
    }
}
