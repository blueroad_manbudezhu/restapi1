<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCodesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**需要验证参数
     * captcha_key,captcha_code
     * @return array
     */
    public function rules()
    {
        return [
            //
            'captcha_key'=>'required|string',
            'captcha_code'=>'required|string'
        ];
    }

    public function messages()
    {
        return [
          'captcha_key.required'=>'请输入图形验证码缓存',
          'catpcha_code.requied'=>'请输入图形验证码'
        ];
    }
}
