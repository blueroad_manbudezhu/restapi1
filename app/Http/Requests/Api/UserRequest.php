<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //根据接口请求动词,设置不同的验证规则
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|string|between:3,25',
                    'password' => 'required|string|min:6',
                    'verify_key' => 'required|string',
                    'verify_code' => 'required|string',
                ];
                break;
            case 'PATCH':
                $userid = auth('api')->id();
                return [
                    'name' => 'required|regex:/^[A-Za-z0-1\-\_]+$/|unique:users,name,' .$userid,
                    'email' => 'email',
                    'introduction' => 'max:80',
                    //exists:images,id,type,avatar,user_id,'.$userId images 表中 id 是否存在，type 是否为 avatar，用户 id 是否是当前登录的用户 id
                    'avatar_image_id' => 'exists:images,id,type,avatar,user_id,' . $userid,
                ];
                break;
        }

    }

    /**用户自定义消息
     * @return array
     */
    public function messages()
    {
        return [
            'name.unique' => '用户名已被占用,请重新填写',
            'name.regex' => '用户名只支持英文、数字、下划线',
            'name.between'=>'用户名必须介于3-25个字符之间',
            'name.requied'=>'用户名不能为空',
        ];
    }
}
