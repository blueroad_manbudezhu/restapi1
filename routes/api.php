<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [
    'namespace' => 'App\Http\Controllers\Api',
    'middleware' => ['serializer:array','bindings']
], function ($api) {
    //登录节流限制
    $api->group([
        'middleware' => 'api.throttle',
        'limit' => config('api.rate_limits.sign.limit'),
        'expires' => config('api.rate_limits.sign.expires'),
    ], function ($api) {
        //图片验证码
        $api->post('captchas', 'CaptchasController@store')->name('api.users.captchas');
        //短信验证码
        $api->post('verifyCodes', 'VerifyCodesController@store')->name('api.users.verifyCodes');
        // 用户注册
        $api->post('users', 'UsersController@store')->name('api.users.store');
        //普通登录
        $api->post('authorizations', 'AuthorizationController@store')->name('api.authorization');
        //社会化登录
        $api->post('socials/{social_type}/authorizations', 'AuthorizationController@weixin')->name('api.socialite');
        //刷新token
        $api->put('authorizations', 'AuthorizationController@update')->name('api.token.refresh');
        //删除token
        $api->delete('authorizations', 'AuthorizationController@destory')->name('api.token.destroy');
    });
    //访问节流设置
    $api->group([
        'middleware' => 'api.throttle',
        'limit' => config('api.rate_limits.access.limit'),
        'expires' => config('api.rate_limits.access.expires'),
    ], function ($api) {
        //不需要登录验证就能访问的接口
        $api->get('categories', 'CategoriesController@index')->name('api.categories.index');
        $api->get('topics','TopicsController@index')->name('api.topics.index');
        $api->get('users/{user}/topics','TopicsController@userindex')->name('api.users.topics.index');
        $api->get('topics/{topic}','TopicsController@show')->name('api.topics.show');
        //某个话题的回复列表
        $api->get('topics/{topic}/replies','RepliesController@index')->name('api.topics.replies.index');
        //某个用户的回复列表
        $api->get('users/{user}/replies','RepliesController@userIndex')->name('api.users.replies.index');
        //资源推荐
        $api->get('links','LinksController@index')->name('api.links');
        $api->get('actived/user','UsersController@activedIndex')->name('api.actived.users.index');
        //需要登录验证才能访问接口
        $api->group(['middleware' => 'auth:api'], function ($api) {
            $api->get('user', 'UsersController@me')->name('api.user.show');
            $api->post('images', 'ImagesController@store')->name('api.user.imageupload');
            $api->patch('user', 'UsersController@update')->name('api.user.update');
            $api->post('topics','TopicsController@store')->name('api.topic.create');
            $api->patch('topics/{topic}','TopicsController@update')->name('api.topics.update');
            $api->delete('topics/{topic}','TopicsController@destroy')->name('api.topics.destroy');
            //回复类api
            $api->post('topics/{topic}/replies','RepliesController@store')->name('api.topics.replies.store');
            $api->delete('topics/{topic}/replies/{reply}','RepliesController@destroy')->name('api.topics.replies.destroy');
            //消息通知api
            $api->get('user/notifications','NotificationsController@index')->name('api.user.notifications.index');
            $api->get('user/notifications/stats','NotificationsController@stat')->name('api.user.notification.stat');
            $api->patch('user/read/notifications','NotificationsController@read')->name('api.user.notification.read');

            //权限管理api
            $api->get('user/permissions','PermissionsController@index')->name('api.user.permissions.index');

        });
    });
});
